# Bayes_Predictive_Distribution

In this rmd file, you can have a look at an exercise using normal laws for a priori and a posteriori predictive distributions. We suppose the weight of student at University 1 which follows a $\mathcal{N}(\theta,\sigma = 10)$, and we have an a priori on $\theta$ : $\theta \sim \mathcal{N}(\mu_0 = 90,\sigma_0 = 20)$. 

1. Simulate realizations of $\tilde x$ in two steps :
    a. Draw of $\theta$
    b. Draw of $\tilde{x}|\theta$

2. Based on the simulations, calculate mean and variance of $\tilde{x}$

3. Compare to the law $\mathcal{N}(\mu_0;\sigma^2 + \sigma_0^2)$

4. What is the a posteriori distribution of $\theta$ ?

5. Give a credibility interval at 95% for $\theta$

6. For a new student of unknown weight $\tilde{x}$, give the predictive distribution of $\tilde x$

7. Find the previous result with a simulation in two steps :
    a. Simulation of $\theta$ according to a posteriori law $p(\theta|{\bf x})$
    b. Simulation of $\tilde{x}$ knowing the value of $\theta$ simulated before

8. Thanks to the function `many_normal_plots` from `TeachBayes` represent on the same plot the two predictive distributions (predictive distribution a priori and a posteriori)

9. In a frequentist approach, what would be the predictive distribution a priori and a posteriori ?


Author : Marion Estoup 

E-mail : marion_110@hotmail.fr

December 2022
